import Stream from './s-stream.js'
import { spec as decorate } from './stags/decorate'
import { proto } from './stags/core'

const of = Stream.of
Stream.of = (...args) => {
    return Object.assign(
        of(...args)
        , proto
    )
}

export default decorate(Object.assign(Stream.of, Stream))