/* globals Proxy, Symbol */
import * as A from './index.js'
import * as P from './pair.js'
import * as superouter from './superouter.js'

const normalizeArgs = s =>
  s
  .split('/')
  .filter(Boolean)
  .join('/')

const normalizePath = s =>
  '/' + normalizeArgs(s)

function routeV2({
  z: route
  , Route
  , rootRoute=null
}){

  const href = (newRoute) =>
    Route.toURL(newRoute)

  function link(theirNewRoute, options={}){
    const newRoute =  theirNewRoute
    return {
      href: href(newRoute)
      ,onclick(e) {
        const nextRoute = {...newRoute, ...options}

        route( nextRoute )
        e.preventDefault()
      }
    }
  }

  const defaultOptions = { replace: false }
  function subroute(type, defaultRoute, theirDefinition, options={}){

    options = { ...defaultOptions, ...options }

    const parentRoute = out
    const parentRouteValue = out()

    const prefix =
      parentRoute.href( A.$.value.args('') ( parentRouteValue ) )

    const definition =
      A.run(
        theirDefinition
        ,P.mapOverObj(
          key => normalizePath(prefix+'/'+key)
        )
      )

    const SubRoute = superouter.type(type, definition)
    let current
    let initialRouteTag = route().tag

    const routeStream = route.$stream
    const z = A.Z({
      stream: routeStream,
      read(){
        if( initialRouteTag != route().tag ){
          routeStream.end(true)
          return current
        }
        let $

        const pathname = rootRoute.toURL( rootRoute() )

        $= SubRoute.matchOr(
          () => defaultRoute(SubRoute), pathname
        )

        if(
          SubRoute.toURL($) != rootRoute.toURL(rootRoute())
        ){
          if( current ) {
            z($)
          }
        }
        current=$

        return $
      },
      write(f){
        if( initialRouteTag != route().tag ){
          routeStream.end(true)
          return current
        }
        const next = f(current)

        const { replace=options.replace } = next

        // global URL thanks to pattern extensions
        const url = SubRoute.toURL(next)

        const nextRoot = rootRoute.matchOr( () => rootRoute(), url)

        rootRoute( { ...nextRoot, replace })

        current = next
        return next
      }
    })

    return routeV2({ z, Route: SubRoute, rootRoute })
  }

  const others = {
    ...Route
    , type: Route
    , link
    , subroute
    , href
    , map: route.$stream.map
  }

  const out = new Proxy(route, {
    get(_, theirKey){
      const key =
        typeof theirKey == 'string'
        && /^\$\d+$/.test(theirKey)
          ? theirKey.slice(1)
          : theirKey

      if ( key == Symbol.toPrimitive ) {
        return toString
      } else if( typeof key == 'string' ){
        if( key in others ) {
          return others[key]
        } else if (key === '$') {
          return route
        }
      }
    }
  })


  if(!rootRoute){
    rootRoute = out
  }

  return out
}

export default routeV2