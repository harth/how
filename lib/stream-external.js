let current
let seq = 0
let id = () => ++seq

const Stream = {
    Data: (initial) => ({
        id: id(),
        children: {},
        value: initial,
        tag: 'Data',
        type: 'Stream'
    }),
    Computation: (f) => ({
        id: id(),
        children: {},
        data: [],
        value: f,
        computed: undefined,
        tag: 'Computation',
        type: 'Stream'
    })
}

function S(f){
    const node = Stream.Computation(f)

    if( current ) {
        current.children[node.id] = node
    }

    function out(){
        return node.computed
    }
    out.node = node
    return out
}

function data(initial){
    const node = Stream.Data(initial)

    function out(...args){
        if( args.length ) {
            node.value = args[0]
            current.children[node.id] = node
        } else {
            current.children[node.id] = node
        }
        return node.value
    }
    if( current ) {
        current.children[node.id] = node
    }
    out.node = node
    return out
}


function tick(root){
    seq = 0
    current = root
    current.children = {}

    if( current.tag == 'Computation' ) {
        current.value()
        console.log('Executed current.value', ''+current.value, current.computed)
    }

    Object.values(current.children).forEach( node => {
        tick(node)
    })
    current = root
}

function root(f){
    const root = S(f)
    current = root.node

    return root.node
}

root(() => {
    console.log('current.children', current.children)
    const a = data(1)
    const b = data(2)
    console.log('current.children', current.children)

    const c = S( () => {
        return a() + b() + b()
    })

    const d = S( () => {
        return c() + c()
    })

    S( () => {
        console.log(c(), d())
    })

    S( () => {
        if( d() < 20 ) {
            a( a() + 1 )
        }
    })

    // console.log(c(), d())
})


// S(() => {
//     a() + b()
// })

// console.log(current)
tick(current)
console.log(current)
// console.log(current)
// tick(current)
// console.log(current)
// console.log(current)