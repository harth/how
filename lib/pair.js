export const of = (a, b) => [a, b]
export const map = f => pair => [pair[0], f(pair[1])]
export const mapL = f => pair => [f(pair[0]), pair[1]]
export const chain = f => pair => f(pair[0])(pair[1])
export const fromEntries = Object.fromEntries
export const toEntries = Object.entries
export const mapEntries = f => xs => xs.map(map(f))
export const mapLEntries = f => xs => xs.map(mapL(f))
export const chainEntries = f => xs => xs.map(chain(f))
export const overObj = f => o => 
    fromEntries(f(toEntries(o)))

export const chainOverObj = f => o => 
    fromEntries( chainEntries(f) ( toEntries(o) ) )

export const mapOverObj = f => o => 
    fromEntries( mapEntries(f) ( toEntries(o) ) )
