/* globals WeakMap */
import CSS from './css/core.js'
import m from 'mithril'

function setCSSProperty(dom, cachedVar, value){
	dom.style.setProperty(
		'--'+cachedVar, value
	)
}

const isStream =
	x => typeof x == 'function' && 'map' in x

const subscriptions = new WeakMap()

function onvalue(dom, cachedVar, value){

	if ( !isStream(value) ) {
		setCSSProperty(dom, cachedVar, value )
	} else {

		if( !subscriptions.has(value) ) {
			subscriptions.set(value, {})
		}

		const streamSubs = subscriptions.get(value)

		if( !cachedVar in streamSubs ) {
			streamSubs[cachedVar] = value.map(
				value => setCSSProperty(dom, cachedVar, value)
			)

			return () => streamSubs[cachedVar].end(true)
		}
	}

}

function componentAdapter(create, update, remove) {
	let el
	return {
		oncreate(vnode){
			const { dom, attrs: { strings, values } } = vnode
			el = dom.parentElement
			create(el)
			update(el, strings, values)
		},
		onupdate(vnode){
			const { attrs: { strings, values } } = vnode
			update(el, strings, values)
		},
		onremove(){
			remove(el)
		},
		view: ({ children }) => m('.css-child', children )
	}
}

const cache = {}

const main = CSS({
	onvalue,
	createElement: m,
	componentAdapter,
	cache
})

const children = CSS({
	onvalue,
	createElement: (...args) => (...children) => m(...args, ...children),
	componentAdapter,
	cache
})

main.children = children

export default main
export { children }