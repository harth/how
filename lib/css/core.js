function setCSSProperty(dom, cachedVar, value){
	dom.style.setProperty(
		'--'+cachedVar, value
	)
}

let i = 1

const _attachStyleToPage = (name, sheet) => {
	const el = document.createElement('style')
	el.innerHTML = sheet
	requestAnimationFrame( () => {
		document.head.appendChild(el)
	})
}

const CacheItem = (name) => ({
	name,
	component: null
})

export default function CSS({
	componentAdapter
	, createElement
	, onvalue=setCSSProperty
	, className=() => 'css-'+(++i)
	// SSR
	, cache={}
	, attachStyleToPage=_attachStyleToPage
}){

	return function css(strings, ...values){

		const key =
			strings
				.join('')
				.split('\n')
				.map( x => x.replace(/\s/g,'') )
				.join(';')

		const existing = key in cache

		const { name } =
			existing
				? cache[key]
				: cache[key] = CacheItem(className())


		if (!existing){
			const remove =
				attachStyleToPage({ name, cache }, `
					${strings.map(
						(x,i, { length }) => {
							x = x.replace(/&/g, '.'+name )
							if( i < length - 1 ) {
								return x + 'var(--'+name+'-'+i+')'
							} else {
								return x
							}
						}
					)
					.join('')
					}
				`)

			cache[key].component = componentAdapter(
				dom => dom.className += ' ' +name
				, (dom, strings, values) => {
					const cleanup = []
					strings.forEach(
						(x, i, { length }) => {
							if( i < length - 1 ) {
								const response = onvalue( dom, `${name}-${i}`, values[i] )

								if( typeof response == 'function' ) {
									cleanup.push(response)
								}
							}
						}
					)
					return () => Promise.all(cleanup.map( f => f() ))
				}
				, remove
			)
		}

		return createElement(cache[key].component, {
			strings, values, name
		})
	}
}

export {
	_attachStyleToPage as attachStyleToPage
}