import CSS from './core.js'
import m from 'mithril'

export default CSS({
	createElement: m,
	componentAdapter: (create, update) => {
		let cleanup = []
		return {
			onremove(){
				cleanup.forEach( f => f() )
			},
			oncreate(vnode){
				const { dom, attrs: { strings, values } } = vnode
				create(dom.parentElement)
				const response = update(dom.parentElement, strings, values )
				if( typeof response == 'function' ){
					cleanup.push(response)
				}
			},
			onupdate(vnode){
				const { dom, attrs: { strings, values } } = vnode
				const response = update(dom.parentElement, strings, values)
				if( typeof response == 'function' ){
					cleanup.push(response)
				}
			},
			view: () => m('i.css-child')
		}
	}
})