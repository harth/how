import CSS from './core.js'
import React from 'react'

export default CSS({
	createElement: React.createElement,
	componentAdapter: (create, update) => {
		function Component({ strings, values }){

			const containerRef = React.useRef(null)

			React.useEffect(() => {
				create(containerRef.current.parentElement, strings, values)
			}, [1])

			React.useEffect(() => {
					update(containerRef.current.parentElement, strings, values)
			})

			return React.createElement('i', {
				ref: containerRef,
				className:"css-child"
			})

		}

		return Component
	}
})