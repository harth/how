/* globals Proxy, Symbol */

const $delete = undefined

const select = $ => o => {
	const results = []
	$( x => {
		results.push(x)
		return x
	}) (o)
	return results
}

const $propOr = otherwise => key => transform => parent => {

	const re_integer =
		/^\d+$/

	const newParent =
		parent == null
		? re_integer.test(key)
			? []
			: {}
		: Array.isArray( parent )
			? parent.slice()
			: { ...parent }

	const existingValue =
		newParent[key] == null
			? otherwise
			: newParent[key]

	const response = transform(existingValue)

	if( response === $delete) {
		delete newParent[key]
		return newParent
	} else {
		newParent[key] = response
		return newParent
	}

}

const $filter = predicate => f => o => {
	return predicate(o) ? f(o) : o
}

const $map = visitor => $ => x =>
	$(visitor(x))

const $flatMap = visitor => f => o => {
	const $ = visitor(o)

	return $(f) (o)
}

const $union = (...$s) => f => o =>
	$s.reduce( (o,$) => $(f) (o), o )

const compose = fns => o =>
	fns.reduceRight( (o, f) => f(o), o)

const $zero = () => o => o

const $values = f => xs =>
	Array.isArray(xs)
		? xs.flatMap( x => {
			const response = f(x)
			if( response === $delete) {
				return []
			} else {
				return [response]
			}
		})
		: Object.keys(xs)
			.reduce(
				(p,n) => {
					const response = f(xs[n])
					if( response === $delete) {
						return p
					} else {
						return { ...p, [n]: response }
					}
				}
				, {}
			)

const $prop = $propOr(undefined)

function Query({
	path=[]
}){

	let $

	function prop(...args){
		$ = $ || compose(path)

		const isGet =
			args.length == 0

		const isOver =
			!isGet
			&& typeof args[0] === 'function'

		return (
			isGet
				? select($)
			: isOver
				? $(args[0])
				: $(() => args[0])
		)
	}

	prop.path = path
	prop.delete = $delete
	prop.drop = $delete
	prop.filter = f => {
		return Query({
			path: path.concat( $filter(f) )
		})
	}

	Object.defineProperty(prop, 'values', {
		get(){
			return Query({
				path: path.concat($values)
			})
		}
	})

	Object.defineProperty(prop, 'zero', {
		get(){
			return Query({
				path: path.concat($zero)
			})
		}
	})

	prop.union = (...$s) => {
		return Query({
			path: path.concat( $union(...$s) )
		})
	}

	prop.insertQuery = query => {
		return Query({
			path: path.concat( query )
		})
	}

	prop.flatMap = visitor => {
		return Query({
			path: path.concat( $flatMap(visitor) )
		})
	}

	prop.map = visitor => {
		return Query({
			path: path.concat( $map(visitor) )
		})
	}

	function toString(){
		// improve later
		return prop.toString()
	}

	const out = new Proxy(prop, {
		get(_, theirKey){
			const key =
				typeof theirKey == 'string'
				&& /^\$\d+$/.test(theirKey)
				? theirKey.slice(1)
				: theirKey

			if ( key == Symbol.toPrimitive ) {
				return toString
			} else if( typeof key == 'string' ){
				if( key == '$') {
					return prop.insertQuery
				} else if( key.startsWith('$') ) {
					return prop[key.slice(1)]
 				} else {
					return out.$( $prop(key) )
				}
			} else {
				return prop[key]
			}
		}
	})

	out.toString = Function.prototype.toString.bind(prop)

	return out
}

const $ =
	Query({
		path: []
		, select
	})

export { $ }