/* globals Proxy, Symbol */
import * as S from './stream.js'
import { $ as Query } from './queries.js'

const Z = ({
  stream: theirStream2
  , query: $=Query

  // optional custom stream
  , read= () => theirStream2()
  , write= f => theirStream2( f(theirStream2()) )
  , notify= f => (S.watch( $() ) (theirStream2)) .map(f)
  , cacheWrites=false
}) => {

  const ourStream = S.of()
  const removed = S.of()

  ourStream.deleted = removed

  let lastGet

  let ignoreNotification = false

  const over = f => {
    ignoreNotification = true

    write(  state => {
      const newState =
        $(
          target => {

            const result = f(target)

            if( cacheWrites ) {
              lastGet = result
            }

            return result
          }
        ) (state)

      return newState
    })

    if( !cacheWrites ) {
      computeGet()
    }

    ourStream(lastGet)
    return lastGet
  }

  const throttled = ms =>
    S.afterSilence(ms) (S.dropRepeats(ourStream))

  const firstGet = () => {
    computeGet()
    get = subsequentGet
    return lastGet
  }

  const subsequentGet = () => {
    return lastGet
  }

  let get = firstGet

  const computeGet = () => {
    [ read() ]
    .map( $() )
    .map(
      results => {
        if( results.length ) {
          // async redraw can mean a view is using Z.get()
          // to retrieve a list item that doesn't exist anymore
          // so we cache the last get
          lastGet = results[0]
          return lastGet
        } else {
          return lastGet
        }
      }
    )
    .shift()
  }

  notify( () => {
    if( !ignoreNotification ) {
      computeGet()
      ourStream( lastGet )
    }
    ignoreNotification = false
  })

  const remove = () => {
    const existing = get()

    write( $($.$delete) )

    removed( existing )
  }

  function prop(...args){
    if( args.length ) {
        if( typeof args[0] == 'function' ) {
            return over(...args)
        } else {
            return over( () => args[0] )
        }
    }
    return get()
  }

  function query(visitor){
    const query = visitor($)

    const z = Z({
      stream: theirStream2
      , read
      , write
      , query
      , notify
      , cacheWrites
    })

    return z
  }

  function mutate(f){
    f(lastGet)
    // notify
    write( () => read() )

    return lastGet
  }

  function mutateSilent(f){
    f(lastGet)
    return lastGet
  }

  let others = {
    delete: remove
    , deleted: removed
    , stream: ourStream
    , query
    , throttled
    , mutate
    , mutateSilent
    , filter: f => query( x => x.$filter(f) )
    , flatMap: f => query( x => x.$flatMap(f) )
    , get values(){
      return query( x => x.$values )
    }

    // add methods to the Z instance
    // todo-james make router use this instead of
    // the custom proxy
    , instance(f){
      others = f(others)
      return out
    }
  }

  const nested = {}

  const out = new Proxy(prop, {
    get(_, theirKey){
      const key =
        typeof theirKey == 'string'
        && /^\$\d+$/.test(theirKey)
        ? theirKey.slice(1)
        : theirKey

      if ( key == Symbol.toPrimitive ) {
        return toString
      } else if( typeof key == 'string' ){
        if( key == '$') {
          return query
        } else if( key.startsWith('$') ) {
          return others[key.slice(1)]
        } else {
          // make simple property access queries cached
          // to avoid memory leaks
          if( key in nested ) {
            return nested[key]
          } else {
            return nested[key] = query( x => x[key] )
          }
        }
      } else {
        return others[key]
      }
    }
  })

  return out
}

export default Z