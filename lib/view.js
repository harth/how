import * as superouter from './superouter.js'
import m from 'mithril'
import bss from 'bss'
import css2 from './css'
import * as A from './index.js'
import routeV2 from './router.js'

const ActiveModule = ({ getState, moduleCache }) => {
  const route$ = A.stream.watch( x => x.route ) (getState)

  const out = A.stream.merge([
    route$, moduleCache
  ])
  .map(
    ([ route, moduleCache ]) => {

      const module =
        A.fromNullable(moduleCache[route.tag])

      const component =
        A.run(
          module,
          A.chain(
            module =>
              module.Main
                ? A.Y(module.Main)
              : module.default
                ? A.Y(module.default)
              : module.view || typeof module == 'function'
                ? A.Y(module)
                : A.N()
          )
        )

      return { module, component }
    }
  )

  return out
}

const Downloaded = A.either('Downloaded')

const ResolveService = download => getState => tell => {
  const route$ = A.stream.watch( x => x.route ) (getState)

  route$.map(
    route => Promise.resolve(download(route))
      .then( Downloaded.Y, Downloaded.N )
      .then( downloaded =>
        tell( downloaded )(route)
      )
  )
}

const ModuleCacheService = download => (getState, moduleCache) => {

  const assoc = ({ route, module }) => moduleCache(
    { ...moduleCache(), [route.tag]: module },
  )

  // Actually download the code
  ResolveService(download)(getState)(
    Downloaded.fold({
      Y: module => route => {
        assoc({
          route, module
        })
      }
      , N: e => route =>
        // eslint-disable-next-line no-console, no-undef
        console.error('Error downloading code', e, route)
    })
  )
}

/* globals addEventListener, history, window */

function meiosis({ toURL, fromURL, getPath, stream, throttle=500 }) {

  const $route = A.$.route

  const throttler = throttle == 0
    ? () => s => s
    : A.stream.throttle(throttle)

  const startURL = url => {
    const popstates = stream()

    popstates.map(
      () => setTimeout(
        () => m.redraw(), 100
      )
    )

    throttler ( A.stream.dropRepeats(url) ).map(url => {
      if (url+'' !== getPath()) {
        const search =
          url.route.search
          || window.location.search

        if( url.route.replace ) {
          history.replaceState({}, "", url+search)
        } else {
          history.pushState({}, "", url+search )
        }
      }
      return null
    })

    addEventListener("popstate", () => popstates(getPath()))

    return popstates
  }

  const start = model$ => {
    const url$ = model$.map(model =>
      [model]
        .flatMap( $route() )
        .map(route => {
          const url = toURL(route)

          return {
            valueOf(){
              return url
            }
            ,route
          }
        })
        .shift()
    )

    return startURL(url$).map(url =>
      [url]
        .map(fromURL)
        .map( x => A.$.route(x) )
        .shift()
    )
  }

  return {
    start
  }
}

const ensureFn = x => typeof x == 'function' ? x : () => x

function RouteMount(vnode) {
    return {
    view: ({ children, attrs: { attrs: getAttrs } }) => {
      const childAttrs = { ...getAttrs() }
      return children.map( f => f(childAttrs) )
    }
  }
}

const defaultRouteError = pathname =>
  'No route was found for url: '
    + pathname
    + '!  Please provide a defaultRoute.'

const componentWithAttrs = globalAttrs => f => () => {
  return {
    view: ({ attrs }) =>
      m(Inline, { ...globalAttrs(),...attrs, view: f })
  }
}

function Inline({ attrs }){
  const session = A.stream.session()
  attrs.stream = session.of

  Object.keys(A.stream).forEach(
    k => attrs.stream[k] = A.stream[k]
  )

  const {
    route
    , state
    , moduleCache
    , activeComponent
    , activeModule
    , v
    , stream
    , view: attrsView
    , ...customAttrs
  } = attrs

  const streamAttrs =
    Object.entries(customAttrs).reduce(
      (p,[k,v]) => ({ ...p,
        [k]: typeof v == 'function'
          ? v
          : A.stream.dropRepeats(session.of(v))
      })
      , {}
    )

  streamAttrs.dom = attrs.stream()

  const view = attrsView({
    route
    , state
    , moduleCache
    , activeComponent
    , activeModule
    , v
    , stream
    , ...streamAttrs
  })

  return {
    onremove: () => session.end()
    ,oncreate({ dom: theirDom }){
      streamAttrs.dom(theirDom)
    }
    ,onupdate({ attrs }){
      Object.entries(attrs).forEach(
        ([k,v]) => k in streamAttrs
          ? streamAttrs[k](v)
          : null
      )
    }
    ,view(attrs){
      return view(attrs)
    }
  }
}

const mount = (
  container,
  {
    Route: theirRoute,
    initial: theirInitial,
    services: theirServices,
    default: theirDefault,
    reducer: theirReducer,
    location: theirLocation,
    streamKey: theirStreamKey,
    render: theirRender
  }
) => {

    // eslint-disable-next-line no-param-reassign
    theirRoute = theirRoute || {
      Home: ['/', null]
    }

    const render =
      theirRender
      || (
        ({ activeComponent, ...attrs }) =>  {
          return activeComponent()
            ? v(activeComponent(), { key: attrs.route().tag, ...attrs})
            : null
        }
      )

    const Route =
      superouter.type(
        'Route'
        , Object.fromEntries(
          Object.entries(theirRoute)
            .map( ([k,[urlPattern]]) => [k,urlPattern])
        )
      )

    const initial = (...args) => {
      const state = theirInitial ? theirInitial(...args) : {}

      if( !state.route ){
        state.route = fromURL(getPath())
      }

      return state
    }

    const location = theirLocation
      // eslint-disable-next-line no-undef
      || typeof window !== 'undefined' ? window.location : null

    const getPath = () => decodeURI(location.pathname)

    const streamKey = theirStreamKey || (({ route }) => route.tag)

    const download =
      Route.fold(
        Object.fromEntries(
          Object.entries(theirRoute)
            .map( ([k,[,download]]) =>
              [k, ensureFn(download)])
        )
      )

    const failedMatchSentinel = {}

    const defaultRoute =
      theirDefault
      ? theirDefault(Route)
      : Route.matchOr( () => failedMatchSentinel , '/')

    if( defaultRoute == failedMatchSentinel ){
      throw new Error(defaultRouteError('/'))
    }

    const toURL = Route.toURL
    const fromURL = route => Route.matchOr(() => defaultRoute, route)

    const reducer = theirReducer || ((x, f) => f(x))

    const Router = meiosis({
      toURL
      , fromURL
      , getPath
      , stream: A.stream.of
    })

    const setState = A.stream.of()
    const active = A.stream.of()
    const moduleCache = A.stream.of({})

    const activeModule = A.stream.watch( x => x.module ) (active)

    const activeComponent = A.stream.watch( x => x.component) (active)

    const getState =
      A.stream.scan
        (initial({ location }))
        (reducer)
        (setState)

    const state =
      A.Z({ stream: getState, read: getState, write: setState })

    const routeStream =
      A.stream.watch(x => x.route) (getState)

    const nullableActiveModule =
      activeModule.map( A.getOr(null) )

    const nullableActiveComponent =
      activeComponent.map( A.getOr(null) )

    function hyperscript(...args){
      if (typeof args[0] == 'function'){
        return m(
          Inline,
          { view: args[0], ...attrs(), ...(args[1] ? args[1] : {}) }
        )
      } else if (args.slice(1).some( x => typeof x == 'function')) {
        const hooks = args.slice(1).filter( x => typeof x == 'function' )

        const attrs =
          typeof args[1] == 'function'
          ? { hook: hooks }
          : { ...args[1], hook: hooks }

        return hyperscript(
          args[0]
          , attrs
          , ...args.slice(2).filter( x => typeof x != 'function' )
        )
      } else {
        const out = m(...args)

        if ( out.attrs && out.attrs.hook ) {
          const hooks = [].concat( out.attrs.hook )
          delete out.attrs.hook
          const oncreate = out.attrs.oncreate
          const onbeforeremove = out.attrs.onbeforeremove
          const ends = []

          out.attrs.onbeforeremove = (vnode) => {
            return ends.reduce( (p, f) => p.then(f), Promise.resolve() )
              .then(
                () => onbeforeremove && onbeforeremove(vnode)
              )
          }

          out.attrs.oncreate = (vnode) => {

            const session = A.stream.session()
            vnode.attrs.stream = session.of

            Object.keys(A.stream).forEach(
              k => vnode.attrs.stream[k] = A.stream[k]
            )
            const hookAttrs = { ...vnode, ...attrs(), ...vnode.attrs }

            hooks.map(
              f => {
                return Promise.resolve(f(hookAttrs))
                  .then( result => {
                    if( result ) {
                      if ( 'end' in result ) {
                        ends.push( () => result.end(true) )
                      } else if ( typeof result == 'function' ) {
                        ends.push( result )
                      }
                    }
                  })
              }
            )

            if( oncreate ){
              oncreate(vnode)
            }
          }
          delete out.hook
        }
        return out
      }
    }
    const v = hyperscript
    v.css = bss
    v.css.helper('desktop', x =>
      v.css.$media('(min-width: 600px)', x)
    )
    v.request = m.request
    v.redraw = m.redraw
    v.component = componentWithAttrs( () => attrs() )

    Object.assign(v, A)

    const attrs = () => {

      const route = routeStream

      const activeModule = nullableActiveModule
      const activeComponent = nullableActiveComponent

      return {
        state

        ,route // stream
        ,moduleCache // stream
        ,activeModule // stream
        ,activeComponent // stream
        ,v
        ,state
        ,route: routeV2({ z: state.route, Route: Route })
      }
    }

  function Main() {
    return {
      oncreate() {
        Router
          .start( getState )
          .map( setState )

        if( theirServices ) {
          theirServices(attrs())
        }

        ModuleCacheService
          (download) (getState, moduleCache)

        ActiveModule({ getState, moduleCache })
          .map(active)

        const jsonEquals = (a,b) => JSON.stringify(a) === JSON.stringify(b)

        A.stream.dropRepeatsWith(jsonEquals) (activeModule).map( () => {
          main.debug && console.log('activeModule redraw')
          m.redraw()
        })

        A.stream.dropRepeatsWith(jsonEquals) (moduleCache).map( () => {
          main.debug && console.log('moduleCache redraw')
          m.redraw()
        })

        A.stream.dropRepeatsWith(jsonEquals) (activeComponent).map( () => {
          main.debug && console.log('activeComponent redraw')
          m.redraw()
        })
      }
      , view: () =>
          m(RouteMount,
            { key: streamKey(getState()), attrs },
            render
          )
    }
  }
  m.mount(container, function App() {
    return {
      view: () => m(Main, attrs)
    }
  })

  return {
    state, Route, route: routeStream, v
  }
}

const main = mount

main.css = bss
main.request = m.request
main.redraw = m.redraw
main.$ = A.$
main.Z = A.Z
main.A = A
main.css = bss
// experimental
main.css2 = css2
export default main