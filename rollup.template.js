import babel from 'rollup-plugin-babel'
import {terser} from 'rollup-plugin-terser'
import resolve from '@rollup/plugin-node-resolve'

export default ({ input, name, file }) => ({
    input,
    plugins: [
        resolve({}),
        // babel()
        // terser()
    ],
    output: {
        file,
        format: 'umd',
        name,
        sourcemap: 'external'
    },
})
