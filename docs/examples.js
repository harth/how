import V from './lib/view.js'


const TodoMVC =
	({ v }) => () => v('h4'+V.css.margin(0), 'TodoMVC')

export default ({ v, route: parent }) => {
	const route =
		parent.subroute('Examples', x => x.List(), {
			List: '/',
			TapePlayer: '/tape-player',
			TodoMVC: '/todo-mvc', // lel
		})

	return () => v('.examples'
		+ V.css`
			display grid
			gap 1em
			padding: ${ route.isList( route() ) ? '1em' : '0em' }
		`
		, route.isList( route() ) && v('h1', 'Examples')
		, !route.isList( route() )
			&& v('.example'
				+ V.css`
					background-color: rgba(0,0,0,0.5);
					backdrop-filter: blur(15px);
					border-radius: 0px 0.25em 0.25em 0.25em;
					box-shadow: 0px 2px 25px -1px rgba(0,0,0,0.6);
					padding: 0.1em;
					display: grid;
					gap 1em;
				`
				.desktop(`
					padding: 1em;
				`)
				, v('h4'+ V.css.margin('0em'), 'Tape Player')
				, route.isTapePlayer( route() )
					&& v(
						({ stream, dom }) => {
							const content = stream('')
							const Flems = stream()

							const dontCache = Date.now()

							route.fold({
								// these won't exist at bundle time...
								// cp examples dist/examples?
								TapePlayer: () =>
									v.request('/examples/tape-player.js?'+dontCache, { extract: x => x.responseText }),
								TodoMVC: () =>
									v.request('/examples/todo-mvc.js?'+dontCache, { extract: x => x.responseText }),
								List: () => Promise.resolve('')
							}) (route())
								.then(content)

							stream.merge([dom, content, Flems]).map(
								async ([dom, content, Flems]) => {
									window.editorInstance =
									Flems(dom, {
										files: [
											{
												name: 'app.js',
												content: content
												.split('\n')
												.map( x => x.replace(/\t{13}/, ''))
												.join('\n')
											}
										],
										links: [{
											name: 'how.view',
											type: 'script',
											url: 'https://how.harth.io/how.view.min.js?'+dontCache
										}],
										theme: 'night'
										, toolbar: false
										, console: false
										, linkTabs: false
										, fileTabs: false
										, middle: 0
									})
								}
							)

							return () =>
								v('.editor'
									+ V.css`
										background-color: white;
									`
									.$nest('.flems',
										V.css`
											min-height: 468px;
										`
										.desktop(`
											min-height: 600px;
										`)

									)
									, v('script', { src: `${window.location.origin}/flems.html`, onload: () => Flems(window.Flems) })
								)
						}
					)
				, route.isTodoMVC( route())
					&& v(TodoMVC, {})

				, v('iframe'
					+ v.css`
							width 100vw;
							height 100vh;
							background-color white;
							padding: 1em;
							box-sizing: border-box;
					`
					.desktop(`
						width 100%
					`)
					,
					{ src:'/examples/annotated/tape-player.html'
					, frameborder: "0"
					, sandbox: ""
					}
				)
			)
		, route.isList(route())
			&& v('.examples'
				+ V.css`
					display: grid;
					gap: 1em;
				`
				.$nest('a', `
					color: white;
				`)
				,v('a', route.link(route.TapePlayer()), 'Tape Player')
				,v('a', route.link(route.TodoMVC()), 'Todo MVC')
			)
	)
}