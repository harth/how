const A = require ('how')

const User =
    A.tags('User', ['LoggedIn', 'LoggedOut'])

const message =
    User.fold({
        LoggedIn: ({ name }) => `Welcome ${name}!`,
        LoggedOut: () => `Goodbye!`
    })

{
[ message(User.LoggedIn({ name: "Bernie" })) // Hello Bernie!
, message(User.LoggedOut()) // Goodbye!
]
}