// import V from './lib/view.js'

export default ({ v, route: parent }) => {
    const route = parent.subroute('Docs', x => x.Home(), {
        Home: '/',
        API: '/api'
    })

    return () => [
        v('h1', 'Documentation')
    ]
}
