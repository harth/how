import V from './lib/view.js'

const resets = {
	header:
		V.css
		.$nest('h1,h2,h3,h4,h5,h6', `
			margin: 0px;
		`)

	,link:
		V.css`
			color: white;
			text-decoration: none;
			transition: 0.2s;
		`
		.$hover(`
			transform: scale(0.9);
		`)
}

const Home = ({
	route: parent, v
}) => {

	const route=
		parent.subroute('Home', x => x.SumTypes(), {
			'SumTypes': `/sum-types`,
			'Queries': `/queries`,
			'Streams': `/streams`,
			'View': '/view',
			'Z': `/z`
		})

	function nav(){
		return v('header'
			+ V.css`
				display: grid;
				grid-auto-flow: column;
				justify-content: space-between;
				top: 0px;
				left: 0px;
				box-sizing: border-box;
				margin-bottom: 6em;
				padding: 0.8em;
			`
			.desktop(`
				padding: 0em;
			`)
			,v('.header'
				+ V.css`
					padding: 0em;
				`
				+ resets.header
				,v('h1'
					+ V.css`
						font-size: 4em;
						margin: 0em;
	
						display: grid;
						grid-auto-flow: column;
						align-items: center;
						gap: 0.2em;
					`
					, 'how'
				)
				,v('p'
					, 'The futuristic and holistic web app toolbox.'
				)
			)
			,v('.links'
				+ resets.header
				+ V.css`
					text-align: right;
					display: grid;
					gap: 1em;
				`
				.$nest('a', resets.link)

				,v('a', parent.link( parent.Docs() ), v('h4', 'Documentation'))
				,v('a', { href: 'https://gitlab.com/harth/how' }, v('h4', 'Repository'))
				,v('a', parent.link( parent.Examples() ), v('h4', 'Examples'))
				,v('a', { href: 'https://discord.gg/JZGrERr' }, v('h4', 'Chat'))
				,v('a', parent.link( parent.Docs({ args: 'api' }) ), v('h4', 'API'))

			)
		)
	}

	function sections(...sections){
		return v('.sections'
			+ V.css`
				margin-top: 0em;
				padding: 0em;
			`
			, sections
		)
	}

	function quickStart({ route }){
		return v('a'
			, { name: 'quick-start'}
			,v('.section'
				+ V.css`
					display: grid;
					position: relative;
					justify-items: center;
					grid-template-columns: 1fr;
					background-color: rgba(0,0,0,0.5);
					backdrop-filter: blur(15px);
					border-radius: 0px 0.25em 0.25em 0.25em;
					box-shadow: 0px 2px 25px -1px rgba(0,0,0,0.6);
					padding: 1em;
					gap: 1em;
				`
				.wide(`
					grid-template-columns: 1fr 1fr;
					justify-items: start;
					gap: 1em;
				`)
				,v('.tabs'
					+ V.css`
						display: grid;
						font-size: 0.8em;
						transform: translateY(-40px);
						width: 100%;
						grid-auto-flow: column;
						align-content: center;
						align-items: center;
						position: absolute;
						top: 0px;
						left: 0px;
					`
					.desktop(`
						font-size: 1em;
						transform: translateY(-51.3px);
					`)
					.$nest('.tab', `
						height: 100%;
						display: grid;
						align-content: center;
						padding: 0em 1em;
						box-sizing: border-box;
						transform-origin: bottom;
						transition: 0.2s;
						cursor: default;
						text-align: center;
					`)
					.$nest('.tab.active', `
						background-color: white;
						color: black;
					`)
					.$nest('.tab.inactive', `
						background-color: black;
						color: white;
					`)
					.$nest('.tab.inactive:hover', `
						background-color: black;
						color: white;
						transform: scale(0.95);
					`)
					,v('a'
						+ V.css`
							display grid;
							align-items: inherit;
							text-decoration: none;
						`
						,route.link( route.SumTypes(), { replace: true })
						,v('h4.tab'
							+ (route.isSumTypes( route() ) ? '.active' : '.inactive')
							+ V.css.margin('1em 0em'), 'Sum Types'
						)
					)
					,v('a'
						+ V.css`
							display grid;
							align-items: inherit;
							text-decoration: none;
						`
						,route.link( route.Streams(), { replace: true })
						,v('h4.tab'
							+ (route.isStreams( route() ) ? '.active' : '.inactive')
							+ V.css.margin('1em 0em')
							, 'Streams'
						)
					)
					,v('a'
						+ V.css`
							display grid;
							align-items: inherit;
							text-decoration: none;
						`
						,route.link( route.Queries(), { replace: true })
						,v('h4.tab'
							+ (route.isQueries( route() ) ? '.active' : '.inactive')
							+ V.css.margin('1em 0em')
							, 'Queries'
						)
					)
					,v('a'
						+ V.css`
							display grid;
							align-items: inherit;
							text-decoration: none;
						`
						,route.link( route.View(), { replace: true })
						,v('h4.tab'
							+ (route.isView( route() ) ? '.active' : '.inactive')
							+ V.css.margin('1em 0em'), 'View'
						)
					)
					,v('a'
						+ V.css`
							display grid;
							align-items: inherit;
							text-decoration: none;
						`
						,route.link( route.Z() )
						,v('h4.tab'
							+ (route.isZ( route() ) ? '.active' : '.inactive')
							+ V.css.margin('1em 0em')
							, 'Z'
						)
					)
				)
				// , v( ({ count=0 }) => () => v('h1', 'hi '+(++count)) )
				, v('.prose'
					+ V.css`
						width: 100%;
						box-sizing: border-box;
						color: #ffffff;
						border-radius: 0.25em;
					`
					.$nest('code', `
						color: #66c7ff;
					`)
					.desktop(
						V.css`
							padding: 0em 1em;
						`
						.$nest('code', `
							font-size: 1.2em;
							
						`)
					)
					,
					{ hook: ({ dom }) =>
						// any stream that's returned by a hook is ended when the vnode unmounts
						route.map(
							async () => {
								const { default: marked } = await import('marked')

								let $
								$= route()
								$= route.fold({
									View: () => '/samples/view-quick-start.md',
									SumTypes: () => '/samples/sum-types-quick-start.md',
									Queries: () => '/samples/sum-types-quick-start.md',
									Streams: () => '/samples/sum-types-quick-start.md',
									Z: () => '/samples/sum-types-quick-start.md',
								}) ($)
								$= await v.request($, {
									extract: x => marked(x.responseText)
								})

								dom.innerHTML = $
							}
						)
					}
				)
				,v('.runkit-container'
					+ V.css`
						overflow: hidden;
						width: 100%;
						min-height: 400px;	
						overflow-y: auto;
						
						align-content: center;
						align-items: center;
					`
					.desktop(`
						max-height: 100%;
					`)
					,v('script', { id: 'runkitScript', src: "https://embed.runkit.com" })
					,v('.runkit-sample'
						,
						{ hook: ({ dom, stream }) => {

							const runkit = stream()

							window.runkitScript.onload = () =>
								runkit(window.RunKit)

							const filepath =
								route.map(
									route.fold({
										View: () => '/samples/view-quick-start.js',
										SumTypes: () => '/samples/sum-types-quick-start.js',
										Queries: () => '/samples/sum-types-quick-start.js',
										Streams: () => '/samples/sum-types-quick-start.js',
										Z: () => '/samples/sum-types-quick-start.js',
									})
								)

							const unformattedSource =
								v.run(
									filepath
									, stream.async( $ =>
										v.request($, { extract: x => x.responseText })
									)
								)

							const source =
								unformattedSource.map(
									$ => ($+'')
										.split('\n')
										.filter(Boolean)
										.slice(1,-1)
										.map( s => s.replace(/^ {4}/g, '') )
										.join('\n')
								)

							let cell
							stream.merge([runkit, source])
								.map(
									async ([ RK, source ]) => {
										if( cell ) {
											await cell.setSource(source)
											await cell.evaluate()
										} else {
											cell =
												RK.createNotebook({ element: dom, source, evaluateOnLoad: true })
										}

									}
								)

							return () => cell.destroy()
						}
					})
				)
			)
		)
	}

	function footer(){
		return v('.footer'
			+ V.css`
				position: fixed;
				left: 0em;
				bottom: -1em;
			`
			, v('p'
				+V.css`
					display: grid;
					grid-auto-flow: column;
					align-items: center;
					gap: 0.5em;
	
					font-size: 1em;
					font-weight: 500;
				`
				, v('a'
					+ V.css`
						display: grid;
						grid-auto-flow: column;
						align-items: center;
						gap: 0.5em;
						
						letter-spacing: -1px;
						text-transform: uppercase;
						font-weight: 700;
	
						padding-right: 0.5em;
						padding: 0.5em;
						background-color: white;
					`
					.desktop(
						`border: none;`
					)
					+ resets.link
					,
					{ href: 'https://harth.io' }
					,logo()
				)
			)
		)
	}

	function logo(){
		return v('.logo'
			+V.css`
				background-color: white;
				max-width: 2em;
				max-height: 2em;
				display: inline-block;
			`
			, v('img'
				+ V.css`
					max-width: 100%;
					display: grid;
				`
				,
				{ src: "https://harth.io/harth-icon.optimized.svg"
				}
			)
		)
	}

	return () => [
		v('.container'
			+ V.css`
				display grid
				justify-content: center;
			`
			,v('.app'
				+ V.css`
					padding: 0em 0em 2.5em 0em;
					max-width: 65em;
				`
				.desktop(`
					padding: 2.5em;
				`)
				.wide(`
					padding: 2em 8em;
				`)
				, nav()
				, sections(
					quickStart({ route })
				)
			)
			, footer()
		)
	]
}

export default Home