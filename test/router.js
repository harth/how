import test from 'tape'
import * as A from '../lib/index.js'
import routeV2 from '../lib/router.js'
import * as superouter from '../lib/superouter.js'

const attrs = ({ pathname, Route }) => {
  const setState = A.stream.of()
  const getState = A.stream.scan({
    route: Route.matchOr( () => null, pathname )
  })
  ( (p,f) => f(p) )
  ( setState )

  const state =
    A.Z({ read: getState, write: setState, stream: getState })

  return {
    state,
    route: routeV2({ z: state.route, Route })
    ,getState, setState
  }
}

const FakeEvent = () => ({
  defaultPrevented: false
  ,preventDefault(){
    this.defaultPrevented = true
  }
})

test('Router Top Level Basics', t => {

  const Root = superouter.type('Root', {
    A: '/',
    B: '/b'
  })

  const { state, route } =
    attrs({ pathname: '/', Route: Root })

  t.equals(
    JSON.stringify(route()), JSON.stringify(state().route)
    , 'route() returns the state of the Z query'
  )


  t.equals(
    JSON.stringify(route.A()), JSON.stringify(Root.A())
    , 'route has all superouter instance methods'
  )

  t.equals(
    route.toURL( route() ), '/'
    , 'e.g. extracting the url'
  )

  route( route.B() )

  t.equals(
    JSON.stringify(route()), JSON.stringify(route.B())
    ,'route( <routeInstance> ) updates state to hold that route'
  )

  t.equals(
    route.link( route.B() ).href
    , route.toURL(route.B())
    , 'route.link( <routeInstance> ) produces a href'
  )

  const fakeEvent = FakeEvent()
  route.link( route.A() ).onclick( fakeEvent )
  t.equals(
    JSON.stringify(route())
    , JSON.stringify(route.A())
    , '/root.link( <routeInstance> ) produces a working on click'
  )

  t.equals(
    fakeEvent.defaultPrevented
    ,true
    ,'the onclick prevents default'
  )

  t.end()
})

test('Subroutes', t => {

  const Root = superouter.type('Root', {
    A: '/',
    B: '/b'
  })

  const pathname = '/page1'
  const { route } = attrs({ pathname, Route: Root })

  const createSubRoute = () =>
    route.subroute('SubRoute', x => x.C(), {
      C: '/',
      D: '/d'
    })

  const subroute = createSubRoute()

  t.equals(
    subroute.toURL( subroute() )
    , pathname
    ,'subroute instances have the same api as their parent route'
  )

  t.equals(
    subroute.link( subroute() ).href
    , route.toURL( route() )
    , 'subroute.link produces global hrefs'
  )

  t.equals(
    typeof subroute.isD, 'function'
    , 'subroute has superouter type methods'
  )

  {
    const { onclick } = subroute.link( subroute.D() )
    const event = FakeEvent()
    onclick( event )
  }

  t.equals(
    JSON.stringify( subroute() )
    , JSON.stringify( subroute.D() )
    , 'subroute.onclick changes subroute state'
  )

  t.equals(
    route().value.args
    , 'd'
    , 'subroute.onclick changes parent route state'
  )

  console.log(subroute.link(subroute()).href)

  t.end()
})

test('Deeply Nested Subroutes', t => {
  const pathname = '/b/d/f'
  const Root = superouter.type('Root', {
    A: '/',
    B: '/b'
  })

  const { route } = attrs({ pathname, Route: Root })

  const subroute =
    route.subroute('SubRoute', x => x.C(), {
      C: '/',
      D: '/d'
    })

  const subsub =
    subroute.subroute('SubRoute', x => x.E(), {
      E: '/',
      F: '/f'
    })

  t.equals(
    JSON.stringify({
      route: route(),
      subroute: subroute(),
      subsub: subsub()
    }, null, 2)
    ,JSON.stringify({
      route: Root.B({ args: 'd/f' }),
      subroute: subroute.D({ args: 'f' }),
      subsub: subsub.F()
    },null,2)
    ,'Nested routes hydrate from the parent args'
  )

  t.end()
})

test('Route / Subroute should produce streams', t => {
  const pathname = '/b/d/f'
  const Root = superouter.type('Root', {
    A: '/',
    B: '/b'
  })

  const { route } = attrs({ pathname, Route: Root })

  const subroute =
    route.subroute('SubRoute', x => x.C(), {
      C: '/',
      D: '/d'
    })

  t.equals(
    typeof route.map,
    'function',
    'route has stream access'
  )

  t.equals(
    typeof subroute.map,
    'function',
    'subroute has stream access'
  )

  t.end()
})


test( 'Queries on the Router use $', t => {

  const Root = superouter.type('Root', {
    A: '/',
    B: '/b'
  })

  const { route } =
    attrs({ pathname: '/', Route: Root })

  t.equals(
    typeof route.C,
    'undefined'
    ,'trying to Query wont work on the route instance'
  )


  t.equals(
    typeof route.$.C,
    'function',
    'Queries allow $ access into state, on the route'
  )

  route.$.C('Queries work'),

  t.equals(
    route.$.C(),
    'Queries work',
    'Queries setting works on the route.'
  )

  t.end()
})