import test from 'tape'
import {$} from '../lib/queries.js'

test('Basics', t => {

    const $abc = $.a.b.c
    const state = $abc(2) ({})
    const state2 = $abc( x => x * 100 ) (state)

    t.equals(state.a.b.c, 2, 'Value was set at right place')
    t.equals($abc() (state)[0], 2, 'Is accessible')
    t.equals(
        JSON.stringify(state)
        , '{"a":{"b":{"c":2}}}'
        , 'Complete obj structure is identical'
    )

    t.equals(
        state2.a.b.c
        , 200
        , 'Value was transformed corrrectly'
    )

    t.end()
})

test('Combinators', t => {

    const state = {
        users: [
            { id: 1
            , color: 'red'
            , age: 15
            }
            ,
            { id: 2
            , color: 'blue'
            , age: 15
            }
            ,
            { id: 3
            , color: 'red'
            , age: 20
            }
        ]
    }

    const users = $.users.$values

    const color = $.color
    const two = users.$filter( x => x.id == 2 )
    const red = users.$filter( x => x.color == 'red' )

    t.equals(
        JSON.stringify(two() (state))
        , JSON.stringify([state.users[1]])
        , 'Queried correct object'
    )

    t.equals(
        JSON.stringify( two.$(color) () (state) )
        , JSON.stringify( [state.users[1].color] )
        , 'Queries can be composed via `a.$(b)`'
    )

    t.equals(
        JSON.stringify(red() (state)),
        JSON.stringify(
            state.users.filter( x => x.color == 'red' )
        )
        ,'Queries can return multiple results'
    )

    t.equals(
        JSON.stringify(
            red( $.age( x => x * 100 ) ) (state)
        )
        , JSON.stringify(
            {
                ...state
                , users:
                    state.users
                    .map(
                        x => x.color == 'red'
                            ? { ...x, age: x.age * 100 }
                            : x
                    )
            }
        )
        , 'Can transform multiple results in one call'
    )

    const MITM =
        users.$flatMap(
            x => x.color == 'blue'
                ? $.$map( x => ({ ...x, hello: 'world' }))
                : $.$zero
        )

    t.equals(
        JSON.stringify(
            MITM( x => x ) (state)
            , null, 2
        ),
        JSON.stringify(
            (
            { ...state
            , users:
                state.users.map(
                    x => x.color == 'blue'
                        ? ({ ...x, hello: 'world' })
                        : x
                )
            }
            )
            , null
            , 2
        )
        , 'Bake a conditional mutation into a query without compiling it'
    )

    const state2 =
        $.friends(
            [{ id: 4, name: 'Ahkmed' }
            ,{ id: 5, name: 'Yolo'}
            ]
        ) (state)

    t.equals(
        JSON.stringify(
            $.$union($.users,$.friends).$values.id () (state2)
        )
        ,JSON.stringify(
            [ ...state2.users.map( x => x.id )
            , ...state2.friends.map( x => x.id )
            ]
        )
        , 'Combine multiple sources into one query via $union'
    )

    const yolo = $.friends.$values.$filter( x => x.id == 5 )


    t.equals(
        JSON.stringify(
            yolo( $.$delete ) (state2).friends
        )
        , JSON.stringify(
            state2.friends.filter( x => x.id != 5 )
        )
        , 'Can delete elements'
    )

    t.end()
})
