import test from 'tape'
import Z from '../lib/z.js'
import { $ } from '../lib/queries.js'
import * as stream from '../lib/stream.js'
import * as S from '../lib/stags/index.js'

test('Z', t => {

    const users = stream.of(S.Y([
        { id: 1, name: 'Tarek' },
        { id: 2, name: 'Sara' }
    ]))

    const query =
        $
            .$(S.map)
            .$values
            .$filter( x => x.id == 2 )

    const emissions = {
        users: [],
        name: [],
        name2: [],
        name3: []
    }

    const z = Z({
        read: () => users()
        ,write: f => users(f(users()))
        , stream: users
        , query: $.$(S.map)
    })

    const user = z.$values.$filter( x => x.id == 2 )

    const name = user.name
    const name2 = user.name
    const name3 = name.$( x => x )

    users.map( x => emissions.users.push( query.name() (x)[0] ) )
    name.$stream.map( x => emissions.name.push(x) )
    name2.$stream.map( x => emissions.name2.push(x) )
    name3.$stream.map( x => emissions.name3.push(x) )

    name('Sas')

    t.equals(
        query.name() (users())[0]
        , name()
    )

    name2('Sassafras')
    name3('Sarah')

    t.deepEquals(
        emissions
        ,
        {
          users: [ 'Sara', 'Sas', 'Sassafras', 'Sarah' ]
          , name: [ 'Sara', 'Sas', 'Sassafras', 'Sarah' ]
          , name2: [ 'Sara', 'Sas', 'Sassafras', 'Sarah' ]
          , name3: [ 'Sara', 'Sas', 'Sassafras', 'Sarah' ]
        }
        , 'Parent and children receive all change without infinite loops'
    )

    user.$delete()


    t.end()
})

test('cacheWrites', t => {

    let xs = stream.of([1,2,3,4,5,6])

    let rawI = stream.of(0)

    let i = stream.merge([xs,rawI]).map(
        ([xs,i]) => i % xs.length
    )

    t.equals(i(), 0)

    t.equals(
        (rawI(45), i()), 3
    )

    let [cached,uncached] = [true, false].map(
        cacheWrites => Z({
            stream: i,
            write: f => rawI( f(rawI()) ),
            cacheWrites
        })
    )

    t.equals(
        cached(), 3, 'Cached value = last i()'
    )

    t.equals(
        uncached(), 3, 'Uncached value = last i()'
    )

    t.equals(
        (uncached(46), uncached()), 4, 'Uncached recomputes get'
    )

    t.equals(
        (cached(46), cached()), 46, 'Cached does not recompute get'
    )

    t.end()
})