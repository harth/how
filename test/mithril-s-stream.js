import S from 's-js'
import stream from '../lib/s-stream-decorated'
import test from 'tape'

const using = S.root

test('...', t => {
    const x1 = stream.of(0)

    t.equals(x1(), 0)

    x1(1)
    t.equals(x1(), 1)
    x1(2)
    t.equals(x1(), 2)

    using( () => {
        S( () => {
            if( x1() != 3 ){
                x1(3)
            }
            t.equals(x1(), 3)
        })

        S( () => {
            t.equals(x1(), 3)
        })
    })

    t.end()
})

test('combine', t => {
    const a = stream.of()
    const b = stream.of()

    using( () => {
        const add = stream.combine(
            (a,b) => a() + b() > 100 ? stream.HALT : a() + b()
            , [a,b]
        )

        t.equals(add.tag, 'Pending', 'combined stream is pending when all values are not set')
        t.equals(typeof add(), 'undefined', 'value is undefined when combined stream is pending')

        a(1)

        t.equals(add.tag, 'Pending', 'combined stream is pending when some values are still penidng')

        b(3)

        t.equals(add.tag, 'Active', 'combined stream is active after all dependencies are Active')
        t.equals(add(), 4, 'Combined stream executed visitor function and stored the result')

        a(2)

        t.equals(add(), 5, 'Updating a dependency updates the combined stream')

        b(1000)

        t.equals(add(), 5, 'When visitor returns HALT, combined stream does not emit a change')
    })

    t.end()
})

test('map', t => {

    const a = stream.of()

    using( () => {
        const b = a.map( x => x + 1 )

        t.equals(b.tag, 'Pending', 'map stream is pending when dependency is Pending')

        a(1)

        t.equals(b.tag, 'Active', 'map stream is active when depedency is active')

        t.equals(a()+1, b(), 'dependent value is updated based on the visitor')
    })

    t.end()
})

test('ap', t => {

    const a = stream.of(x => x + 1)
    const b = stream.of(1)

    using( () => {
        const c = a.ap(b)

        t.equals(c(), 2, 'ap works')
    })

    t.end()
})

test('merge', t => {

    const a = stream.of(3)
    const b = stream.of(1)

    using( () => {
        const ab = stream.merge([a,b]).map(
            ([a,b]) => a + b
        )

        t.equals(ab(), 4, 'merge works')
    })

    t.end()
})

test('scan', t => {
    const a = stream.of()

    const b = stream.scan((a,b) => a + b, 0, a)

    a(1)

    t.equals(b(), 1, 'scan pt 1')
    a(1)
    t.equals(b(), 2, 'scan pt 2')
    a(1)
    t.equals(b(), 3, 'scan pt 3')

    t.end()
})

test('sum types', t => {
    const f = stream.fold({
        Pending: () => 0,
        Active: x => x
    })

    t.equals( f( stream.of() ), 0, 'fold over pending stream' )
    t.equals( f( stream.of(100) ), 100, 'fold over active stream')


    t.equals(
        stream.isPending(stream.of())
        , true
        , 'generated stags features automatically added'
    )

    t.end()
})

test('default export is a function, mithril-style', t => {

    const x = stream(1)

    t.equals(x+'', 'Stream.of.Active(1)', 'stream(1) same as stream.of(1)')

    t.end()
})