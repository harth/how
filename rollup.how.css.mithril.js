import config from './rollup.template.js'

export default config({
    input: './lib/css/mithril.js',
    file: './dist/how.css.mithril.min.js',
    name: 'css'
})
