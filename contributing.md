#### Website

First run, or after any dependency change run `bash run install` (why? we use [snowpack](https://www.snowpack.dev/))

Run `bash run dev` to spin up a dev server.

If you have permissions use `bash run deploy` to deploy the docs site.

If you use vs code, you can run `bash run dev` then click the debug option `Browser Preview: Launch` but first you must have the following extensions installed:

- Debugger For Chome
- Browser Preview

##### Examples

You can refine the examples by editing within the flems on localhost or the deployed site and then paste into /examples/<example> locally.

Upon deploy the flems will prefill with your src.

#### Docs

Edit the `readme.md` to your hearts content!

#### JS

Very early days so no formal rules yet.  But basically TC39: 👎, FP: 👍

#### Hacks

At the moment the project is considered `unstable`, so there's a bash job: `...` which just add's all, commits with message: '...' and pushes.
One day we'll remove that function, but at the moment a lot of commits boil down to "get stuff working" and the shorthand for that is `...`.

> 🤓 Deployment uses bash run .... with 4 dots instead of 3.  But you'll need AWS access to use that command.

