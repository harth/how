#### What

An optional one sentence to a paragraph explanation on what the proposed change is. 

As multiple parallel PR's stack up, it's helpful to record the effects of a PR.  Sometimes it is difficult to capture all the changes just in the title.

#### Why

A sentence to a paragraph describing the context the made this change important / valuable or necessary.  It makes it easier when queueing up releases to prioritize what changes should be merged and deployed together and in what order.

#### How

> 💡 Optional

Completely optional knowledge sharing section.  Can be helpful for institutional knowledge to explain how the eventual decisions were reached.  

A great way to avoid [Chesterton's Fence](https://en.wikipedia.org/wiki/Wikipedia:Chesterton%27s_fence)

#### How to test

The most important part of the Pull Request.  How does the reviewer determine the changes work as intended.  Should be more detailed as the scope of the PR grows.

#### Questions

> 💡 Optional

There may or may not be questions raised during development of the pull request that are worth resolving before merge or in a future issue.  

#### Fixes

If there are any issues this PR fixes, it's good to reference them using the [default-closing-patterns](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#default-closing-pattern)

If there aren't any issues, perhaps create them before changing your PR from being a WIP.

