import {terser} from 'rollup-plugin-terser'
import resolve from '@rollup/plugin-node-resolve'
import commonjs from '@rollup/plugin-commonjs'
import fs from 'fs'
import path from 'path'

const input = path.resolve('./docs/index.js')

export default {
    input,
    plugins: [
        resolve({}),
        commonjs(),
        terser(),
        {
            // https://twitter.com/Rich_Harris/status/1056558097454514177
            generateBundle(opts, bundle){
                // console.log('generateBundle', opts, bundle)
                // console.log( Object.keys(bundle) )
                const entry =
                    Object.entries(bundle)
                        .filter( ([, x]) => input in x.modules )
                        .map( ([k]) => k )
                        .slice(0,1)

                entry.map(
                    entry => `
                    <!DOCTYPE html>
                    <html lang="en">

                    <head>
                        <meta charset="UTF-8">
                        <meta name="viewport" content="width=device-width, initial-scale=1.0">
                        <title>how.js</title>
                        <link rel='manifest' href='/manifest.json'>
                        <style>
                            @import url('https://rsms.me/inter/inter.css');

                            html {
                                font-family: 'Inter', sans-serif;
                                opacity: 0;
                                transition: opacity 1s;
                            }

                            html.loaded {
                                opacity: 1;
                            }

                            @supports (font-variation-settings: normal) {
                                html {
                                    font-family: 'Inter var', sans-serif;
                                }
                            }
                        </style>
                        <script>
                            let ready=
                                ('fonts' in document)
                                    ? new Promise(
                                        Y => document.fonts.onloadingdone=Y
                                    )
                                    :new Promise(Y => setTimeout(Y,1500))

                            ready.then(
                                () => document.body.parentElement.classList.add('loaded')
                            )
                        </script>
                        <script>
                            // This is the "Offline copy of pages" service worker

                            // Add this below content to your HTML page, or add the js file to your page at the very top to register service worker

                            // Check compatibility for the browser we're running this in
                            if("serviceWorker" in navigator) {
                                if(navigator.serviceWorker.controller) {
                                    console.log("[PWA Builder] active service worker found, no need to register");
                                } else {
                                    // Register the service worker
                                    navigator.serviceWorker
                                        .register("sw.js",{
                                            scope: "./"
                                        })
                                        .then(function(reg) {
                                            console.log("[PWA Builder] Service worker has been registered for scope: "+reg.scope);
                                        });
                                }
                            }

                        </script>
                        <script src="/${entry}" type="module"></script>
                    </head>
                    </html>
                    `
                )
                .map( s =>
                    s
                        .split('\n')
                        .map( x => x.replace(/^\s{24}/, '') )
                        .join('\n')
                )
                .forEach(
                    html =>
                        fs.writeFileSync(
                            './dist/docs/index.html'
                            , html.trim()
                            , 'utf8'
                        )
                )
            }
        }
    ],
    output: {
        dir: './dist/docs',
        entryFileNames: '[name].[hash].js',
        chunkFileNames: '[name].[hash].js',
        format: 'esm',
        name: 'docs',
        sourcemap: 'external'
    }
}
